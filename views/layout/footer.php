		<footer class="footer">
			Copyright &copy; 2009-2015 The KPI Institute Ltd. All Rights Reserved
		</footer>
		
		<script type="text/javascript" src="assets/scripts/jquery-3.6.0.min.js"></script>
		<script type="text/javascript">
			// get phone prefix by country code
			$('#country').on('change keyup', function() {
				let country = $(this).val();
				console.log('country', country)
				if (country && country.length >= 2) {
					$.ajax({
						url: 'controllers/PhoneController.php',
						type: 'post',
						data: {
							country: country
						},
						success: function(result) {
							$('#prefix').val('+' + result);
						}
					});
				}
				else {
					$('#prefix').val('');
				}
			});

			// download file after submit form
			let download = "<?php echo $_SESSION['download'] ?? false; ?>";
			if (download) {
				window.location = 'controllers/PreviewRequestController.php';
			}
		</script>

	</body>
</html>