<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" type="text/css" href="assets/styles/app.css">

		<title>KPI Institute - TKI Research Library</title>
	</head>

	<body>
		<header>
			<div class="logo">
				<img src="assets/images/logo.png" alt="THE KPI INSTITUTE">
			</div>
			<div class="header">
				<div class="container">
					<div class="page-title">
						<img src="assets/images/Icon1_19.png" alt="">
						<div class="text-primary">TKI RESEARCH LIBRARY</div>
					</div>
					<div class="auth">
						<a href="#">Log In</a>
						<span class="separator"></span>
						<a href="#">Register</a>
					</div>
				</div>
			</div>
		</header>
