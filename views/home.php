<?php
	session_start();
	include_once "layout/header.php";
?>

		<div class="content">
			<div class="container">
				<div class="title">
					The KPI Dictionary Volume I: Functional Areas
				</div>
				<div class="subtitle">
					<div>3,200+ Key Performance Indicator Definitions</div>
					<div>for an in depth view on performance measurement</div>
				</div>

				<div class="description">
					<b>The KPI Dictionary - Volume 1</b> is a reference collection of Key Performance Indicators, used in practice today, which covers the major functional areas of an organization, regardless of its industry specifics. The collection contains the most relevant means for measuring performance, throughout the most common departments in either public, private, or non-profit organizations, such as: Finance, Sales, Human Resources, Marketing, and IT.
				</div>

				<div class="benefit-wrapper">
					<div class="benefit">
						<div class="benefit-title">
							<img src="assets/images/Icon1_03.png" alt="">
							KEY BENEFITS OF THE FUNCTIONAL AREAS KPI DICTIONARY
						</div>
						<div class="benefit-key">
							<ul>
								<li>
									<span class="key text-primary">SELECT</span>
									- the critical KPIs to drive operational success
								</li>
								<li>
									<span class="key text-primary">ACCESS</span>
									- a full collection of KPIs, specific for each functional area
								</li>
								<li>
									<span class="key text-primary">CONFIGURE</span>
									- your own customized departmental scorecard and dashboard
								</li>
								<li>
									<span class="key text-primary">IMPROVE</span>
									- your current KPI framework
								</li>
								<li>
									<span class="key text-primary">BUILD</span>
									- your internal KPI library
								</li>
							</ul>
							<img src="assets/images/Icon1_07.png" alt="">
						</div>
					</div>

					<div class="download">
						<div class="card">
							<div class="download-title text-primary">DOWNLOAD A <b>FREE</b> PREVIEW</div>
							<div>
								To proceed with your order, please take a moment to fill in the fields below:
							</div>

							<div class="form">
								
								<?php
									// dispaly success message
									if (isset($_SESSION['success'])) {
										echo '<div class="alert-success">' . $_SESSION['success'] . '</div>';
										$_SESSION['success'] = null;
									}

									// dispaly error messages
									if (isset($_SESSION['errors']) && count($_SESSION['errors']) > 0) {
										echo '<div class="alert-danger">';
										foreach ($_SESSION['errors'] as $key => $error) {
											echo "<div> $error </div>";
										}
										echo '</div>';

										$_SESSION['errors'] = null;
									}

								?>

								<form action="controllers/PreviewRequestController.php" method="POST">
									<input type="text" name="first_name" placeholder="First Name" required class="form-control">
									<input type="text" name="last_name" placeholder="Last Name" required class="form-control">
									<input type="text" name="company" placeholder="Company" class="form-control">
									<input type="text" name="country" placeholder="Country" class="form-control" id="country">
									<div class="form-group">
										<input type="text" name="prefix" placeholder="Prefix" required class="form-control prefix" id="prefix">
										<input type="text" name="phone" placeholder="Phone Number" pattern="[0-9]+" required class="form-control">
									</div>
									<input type="email" name="email" placeholder="Email Address" required class="form-control">

									<div class="privacy">
										<i>We value your privacy and will never disclose your data to third parties without yout consent.</i>
									</div>

									<input type="submit" value="DOWNLOAD" class="btn btn-primary">
								</form>
							</div>
						</div>
					</div>

					<div class="download-arrow-wrapper">
						<div class="download-arrow-triangle-bottom-right"></div>
						<div class="download-arrow-body text-white">
							DOWNLOAD YOUR PREVIEW
						</div>
						<div class="download-arrow-triangle-right"></div>
						<div class="download-arrow-chevron-top"></div>
						<div class="download-arrow-chevron-bottom"></div>
					</div>
				</div>
			</div>
		</div>

		<div class="tki">
			<div class="container">
				<div class="title text-primary">TKI FACTS AND FIGURES</div>
				<div class="tki-content">
					<div class="count-item">
						<img src="assets/images/Icon1_11.png" alt="">
						<div class="count text-primary">75,000+</div>
						<div>COMMUNITY MEMBERS</div>
					</div>
					<div class="count-item">
						<img src="assets/images/Icon1_13.png" alt="">
						<div class="count text-primary">240+</div>
						<div>RESOURCES PROVIDED</div>
					</div>
					<div class="count-item">
						<img src="assets/images/Icon1_15.png" alt="">
						<div class="count text-primary">20+</div>
						<div>BUSINESS ANALYSTS</div>
					</div>
					<div class="count-item">
						<img src="assets/images/Icon1_17.png" alt="">
						<div class="count text-primary">12+</div>
						<div>PRACTICE DOMAINS</div>
					</div>
					<div class="count-item">
						<img src="assets/images/Icon1_19.png" alt="">
						<div class="count text-primary">11+</div>
						<div>YEARS OF RESEARCH</div>
					</div>
				</div>
			</div>
		</div>

		<div class="contact text-white">
			<div class="contact-item">
				<img src="assets/images/chatus_btn.png" alt="">
				<div>
					<div class="title">CHAT WITH US</div>
					<div>
						Have questions? A customer service representative is online to help you via Live Chat. Join in the conversation.
					</div>
				</div>
			</div>
			<div class="contact-item">
				<img src="assets/images/callus_btn.png" alt="">
				<div>
					<div class="title">CALL US</div>
					<div>
						Get in touch with us. For immediate assistance, we're available to take your call at +61 3 9028 2223.
					</div>
				</div>
			</div>
			<div class="contact-item">
				<img src="assets/images/emailus_btn.png" alt="">
				<div>
					<div class="title">EMAIL US</div>
					<div>
						We'd love to help. Send us an email at office@kpiinstitute.org and a representative will get back to you.
					</div>
				</div>
			</div>
		</div>

<?php
	include_once "layout/footer.php";
?>
