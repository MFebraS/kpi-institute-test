<?php

class PreviewRequest
{
	private $connection;
	private $table_name = "preview_requests";

	// properties
	public $id;
	public $first_name;
	public $last_name;
	public $email;
	public $phone;
	public $country;
	public $company;
	public $created_at;

	public function __construct($db)
	{
		$this->connection = $db;
	}

	function create()
	{
		// query
		$query = "INSERT INTO
					" . $this->table_name . "
				SET
					first_name=:first_name, last_name=:last_name, email=:email, phone=:phone, country=:country, company=:company, created_at=:created_at";

		$stmt = $this->connection->prepare($query);

		// posted values
		$this->first_name = htmlspecialchars(strip_tags($this->first_name));
		$this->last_name = htmlspecialchars(strip_tags($this->last_name));
		$this->email = htmlspecialchars(strip_tags($this->email));
		$this->phone = htmlspecialchars(strip_tags($this->phone));
		$this->country = strtoupper(htmlspecialchars(strip_tags($this->country)));
		$this->company = htmlspecialchars(strip_tags($this->company));
		$this->created_at = date('Y-m-d H:i:s');

		// bind values 
		$stmt->bindParam(":first_name", $this->first_name);
		$stmt->bindParam(":last_name", $this->last_name);
		$stmt->bindParam(":email", $this->email);
		$stmt->bindParam(":phone", $this->phone);
		$stmt->bindParam(":country", $this->country);
		$stmt->bindParam(":company", $this->company);
		$stmt->bindParam(":created_at", $this->created_at);

		if($stmt->execute()) {
			return true;
		}
		else {
			return false;
		}
	}
}