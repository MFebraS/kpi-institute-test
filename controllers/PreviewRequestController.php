<?php
	use PHPMailer\PHPMailer\PHPMailer;
	use PHPMailer\PHPMailer\SMTP;
	use PHPMailer\PHPMailer\Exception;

	require_once '../database/Database.php';
	require_once '../models/PreviewRequest.php';
	require_once '../vendor/autoload.php';

	session_start();

	if ($_SESSION['download']) {
		$_SESSION['download'] = false;

		$file = '../assets/files/TKI-Membership-Brochure.pdf';

		header('Content-Description: File Transfer');
		header('Content-Type: application/pdf');
		header('Content-Disposition: attachment; filename="'. basename($file) .'"');

		readfile ($file);
		exit(); 
	}
	else if($_POST) {
		// form validation
		$errors = [];
		if (empty($_POST['first_name'])) {
			$errors[] = 'First Name is required.';
		}
		if (empty($_POST['last_name'])) {
			$errors[] = 'Last Name is required.';
		}
		if (empty($_POST['email'])) {
			$errors[] = 'Email Address is required.';
		}
		if (empty($_POST['prefix'])) {
			$errors[] = 'Phone Prefix is required.';
		}
		if (empty($_POST['phone'])) {
			$errors[] = 'Phone Number is required.';
		}

		if (count($errors) > 0) {
			$_SESSION['errors'] = $errors;
		}
		else {
			// get database connection
			$database = new Database();
			$db = $database->getConnection();

			// pass connection to models
			$previewRequest = new PreviewRequest($db);

			$phone = $_POST['prefix'] . $_POST['phone'];

		    // set property values
		    $previewRequest->first_name = $_POST['first_name'];
		    $previewRequest->last_name = $_POST['last_name'];
		    $previewRequest->email = $_POST['email'];
		    $previewRequest->phone = $phone;
		    $previewRequest->country = $_POST['country'];
		    $previewRequest->company = $_POST['company'];

		    if($previewRequest->create()){
		        // send email
		        $mail = new PHPMailer(true);
				try {
				    // server settings
				    $mail->isSMTP();
				    $mail->Host       = 'smtp.gmail.com';
				    $mail->SMTPAuth   = true;
				    $mail->Username   = 'febra.dev@gmail.com';
				    $mail->Password   = 'wpvxqblpjycjkwtr';
				    $mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;
				    $mail->Port       = 465;

				    // recipients
				    $mail->setFrom('febra.dev@gmail.com', 'Mailer');
				    $mail->addAddress($_POST['email']);

				    // content
				    $mail->isHTML(true);
				    $mail->Subject = 'The KPI Dictionary - Volume 1';
				    $mail->Body    = '<div>First Name: '. $_POST['first_name'] .'</div>'.
				    	'<div>Last Name: '. $_POST['last_name'] .'</div>'.
				    	'<div>Email: '. $_POST['email'] .'</div>'.
				    	'<div>Phone: '. $phone .'</div>'.
				    	'<div>Country: '. $_POST['country'] .'</div>'.
				    	'<div>Company: '. $_POST['company'] .'</div>';

				    $mail->send();
				} catch (Exception $e) {
				    echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
				    die();
				}

		        $_SESSION['success'] = 'Successfully request preview.';
		        $_SESSION['download'] = true;
		    }
		    else{
		        $_SESSION['errors'] = 'Failed to request preview.';
		    }
		}

		header('Location: ../index.php');
		exit();
	}