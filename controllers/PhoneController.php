<?php
	require_once '../vendor/autoload.php';

	if($_POST && !empty($_POST['country'])) {
		$country = strtoupper($_POST['country']);
		$phoneNumberUtil = \libphonenumber\PhoneNumberUtil::getInstance();
		echo $phoneNumberUtil->getCountryCodeForRegion($country);
	}
