# KPI Institute Test

# Installation
- Move project to localhost htdocs (or your local server)
- Open terminal and go to project directory
- Run `composer install`
- Create new MySQL database with name `kpi_institute_test`
- Import database from database/kpi_institute_test.sql
- Access web in browser `localhost/kpi-institute-test`
